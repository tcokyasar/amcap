from Cut_generation_fn import *
from data_generator_multi import *
from Travel_time_calc import *
from datetime import datetime
import numpy as np
import pandas as pd
import googlemaps
import multiprocessing

### BEGIN PROVIDING ESSENTIAL PARAMETERS ###
P = 4                 #Total number of products
I = 2                  #Total number of locations
S = 2                  #Max of AM machine numbers
seedno = 1              #Seed number to generate data
lambdalower = 8         #Lower bound of part arrival rate
lambdaupper = 10        #Upper bound of part arrival rate
M1 = 10000000
M2 = 10000000
M3 = 50000000
M4 = 50000000
totaltimelimit = 3600
desired_gap = 0.0005
### END PROVIDING ESSENTIAL PARAMETERS ###

#Run the following to get the travel time
(travel_time,mile_dist) = Traveltimefn(I,seedno)

### BEGIN GENERATING THE DATA ###
(CF,CA,CI,CL,h,d,lmbda,mu,tauA,tauI,P_sub,
 mass) = data_generator(P,S,I,seedno,lambdalower,lambdaupper,travel_time)
(Sstar,B) = Sstar_and_B(P,d,h,lmbda,tauI)
FI = FI_calculator(CI,h,Sstar,lmbda,tauI,d,B,P)
FA = FA_calculator(CA,CL,lmbda,mu,tauA,d,P,I)
### END GENERATING THE DATA ###

comptime = datetime.now()

(x,y,rho,Wq,solutions) = Cut_Generation(FA,FI,CF,CA,CI,CL,h,d,
            lmbda,mu,tauA,tauI,P_sub,P,I,S,Sstar,B,
            M1,M2,M3,M4,totaltimelimit,seedno,desired_gap)

##########################################################################################################
totalcomptime = (datetime.now()-comptime).total_seconds()
print('\x1b[0;35;46m' + 'The total solution time is %s seconds'%(totalcomptime) + '\x1b[0m')