from datetime import datetime
import numpy as np
import pandas as pd
# import googlemaps
import billiard as multiprocessing
import csv

from haversine import haversine


# def travel(pair_origin_destination):
#     gmaps = googlemaps.Client(key='my_key')
#     (origin,destination) = pair_origin_destination
#     directions_result = gmaps.directions(
#     (origin[0],origin[1]),
#     (destination[0],destination[1]),
#     mode="driving", avoid="ferries", departure_time=datetime.now())
#     return(directions_result[0]['legs'][0]['duration']['value']/60/60/24/365.25*2,
#            directions_result[0]['legs'][0]['distance']['value']/1609)

def travel(pair_origin_destination):
    (origin,destination) = pair_origin_destination
    dist = haversine(origin,destination, unit='mi')
    time = haversine(origin,destination, unit='mi')/30
    return(time,dist)
    
def Traveltimefn(I,seedno):
    ### BEGIN CALCULATING DISTANCE AND TRAVEL TIME ###
    np.random.seed(seedno)
    I_list = list(np.random.randint(0,3229,I))
    print("Reading csv file for location info...\n")
    with open('2014_us_cities.csv', mode ='r') as file:
        all_locs = [l for l in csv.reader(file)][1:]

    print("Getting travel time based on Haversine Distances...\n")
    param_list = {(i+1,j+1):((float(all_locs[I_list[i]][2]),float(all_locs[I_list[i]][3])),
                   (float(all_locs[I_list[j]][2]),float(all_locs[I_list[j]][3])))
                 for i in range(I) for j in range(I)}
    
    if len(param_list)>int(multiprocessing.cpu_count()):
        cpucnt = int(multiprocessing.cpu_count())-1
    else:
        cpucnt = len(param_list)
    process = multiprocessing.Pool(cpucnt) #Use multiprocessing.cputcount-1 for local work
    result = process.map(travel, param_list.values()); process.close()
    (trtime, trdist) = ([i[0] for i in result], [i[1] for i in result])

    travel_time = {list(param_list.keys())[i]:trtime[i] for i in range(len(trtime))}
    mile_dist = {list(param_list.keys())[i]:trdist[i] for i in range(len(trdist))}
    ### END CALCULATING DISTANCE AND TRAVEL TIME ###
    return(travel_time,mile_dist)
