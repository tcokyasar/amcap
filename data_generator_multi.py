import math
import numpy as np


### BEGIN GENERATING DATA ###
def data_generator(P,S,I,seedno,lambdalower,lambdaupper,travel_time):
    print("Loading preliminary data...\n")
    year = 365; hour = 24; np.random.seed(seedno)
    
    ### BEGIN PARTITIONING P INTO LOCATIONS ###
    indices = np.arange(1, P); np.random.shuffle(indices); indices = indices[:I - 1]; indices.sort()
    P_sub = {i+1:list(np.split(np.arange(1, P+1), indices)[i]) for i in range(I)}
    ### END PARTITIONING P INTO LOCATIONS ###
    
    ### BEGIN CREATING tauA ###
    tauA = {(p,j):travel_time[j,i] for i in range(1,I+1) 
            for p in P_sub[i] for j in range(1,I+1)}
    ### END CREATING tauA ###
    
    CF = {i:120000 for i in range(1, I+1)} #120000 Annual fixed cost (machine and maintenance)
    #CA Estimation
    OOO = 20*1.15          #Machine operator cost per hour ($/h)  #Euro=1.15USD
    AAA = 1.2              #Set-up time per build (h)
    BBB = 3                #Post-processing time per build (h)
    HT = 20*1.15           #Heat treatment cost per build ($)  #Euro=1.15USD
    AP = (OOO*AAA)         #Pre-processing cost per part ($)
    BP = (OOO*BBB+HT)      #Post-processing cost per part ($)
    VVV, gamma, DDD, MMM, mass, MP, CA = {}, {}, {}, {}, {}, {}, {}
    CI, lmbda, tauI, d, h, mu, CL = {}, {}, {}, {}, {}, {}, {}
    for p in range(1,P+1):
        VVV[p] = np.random.uniform(100, 10000)  #Part volume in cm3
        gamma[p] = np.random.uniform(0.1, 10)   #CI=gamma*CA i.e., CA multiplier
        DDD[p] = np.random.uniform(2.68, 8.9)   #Density of sintered mat. (g/cm3)
        MMM[p] = 50*DDD[p]                      #Material cost per kg ($/kg)
        mass[p] = DDD[p]*1.1*VVV[p]             #mass := Mass of material per part (g)
        MP[p] = mass[p]/1000*MMM[p]             #MP := Material cost per part ($)
        CA[p] = MP[p] + AP + BP                 #CA := production cost per unit ($)
        CI[p] = CA[p]*gamma[p]                  #CI=gamma*CA i.e., procurement cost per unit ($)
        lmbda[p] = np.random.uniform(lambdalower, lambdaupper) #Demand rate
        tauI[p] = np.random.uniform(10, 18)/year  #Procurement lead time
        while tauI[p]*lmbda[p] >= 1:
            tauI[p] = np.random.uniform(low=10, high=18)/year #orig. low=20/year, high=60/year
        d[p] = np.random.uniform(250, 1000)*year*hour #Unavailability cost per unit per time (annual)
        h[p] = CI[p]*0.25                    #Inventory holding cost per unit per time (annual)
        mu[p] = 1/(VVV[p]*(1/70)/year/hour)  #Build rate per time (annual)
        for i in range(1, I+1):
            CL[p,i] = 40 + VVV[p]*DDD[p]*0.0610237*0.0361273*0.5*tauA[p,i]*year

    ### BEGIN SETTING CL FOR PRODUCTS SELF-SOURCED ###
    for i in range(1,I+1):
        for p in P_sub[i]:
            CL[p,i] = 0
    ### END SETTING CL FOR PRODUCTS SELF-SOURCED ###
    return(CF,CA,CI,CL,h,d,lmbda,mu,tauA,tauI,P_sub,mass)
### END GENERATING DATA ###

### BEGIN CALCULATING S* AND B ###
def Sstar_and_B(P,d,h,lmbda,tauI):
    def leftcalc(n,lam,ta):
        number = sum((((lam*ta)**nn)/math.factorial(nn))*
                math.exp(-1*lam*ta) for nn in range(0, n+1))
        return(number)
    def Pscalc(Sstarval, lam, ta):
        if Sstarval <= 0:
            Psval = 1
        else:
            Psval = (1 - sum((((lam*ta)**n)/math.factorial(n))*math.exp
                          (-1*lam*ta) for n in range(0, Sstarval))) #Sstarval here refers to S-1
        return(Psval)
    def Bcalc(Sstarval, lam, ta):
        return(lam*ta*Pscalc(Sstarval-1, lam, ta) - Sstarval*Pscalc(Sstarval, lam, ta))
    right = {p:d[p]/(d[p]+h[p]) for p in range(1, P+1)}
    n = {p:0 for p in range(1, P+1)}
    Sstar = {p:0 for p in range(1, P+1)}
    leftvals = {p:0 for p in range(1, P+1)}
    for p in range(1, P+1):
        while leftcalc(n[p],lmbda[p],tauI[p]) < right[p]:
            n[p]+=1
        leftvals[p] = leftcalc(n[p],lmbda[p],tauI[p])
        Sstar[p] = n[p]
    B = {p:Bcalc(Sstar[p], lmbda[p], tauI[p]) for p in range(1, P+1)}
    return(Sstar,B)
### END CALCULATING S* AND B ###

### BEGIN CALCULATING FI ###
def FI_calculator(CI,h,Sstar,lmbda,tauI,d,B,P):
    FI = {p:(CI[p]*lmbda[p] + h[p]*(Sstar[p]
        -lmbda[p]*tauI[p])+(h[p]+d[p])*B[p])
         for p in range(1, P+1)}
    return(FI)
### END CALCULATING FI ###

### BEGIN CALCULATING FA ###
def FA_calculator(CA,CL,lmbda,mu,tauA,d,P,I):
    FA = {(p,i):lmbda[p]*(CA[p]+CL[p,i]+d[p]*(tauA[p,i] + 1/mu[p])) 
          for p in range(1, P+1) for i in range(1, I+1)}
    return(FA)
### END CALCULATING FA ###