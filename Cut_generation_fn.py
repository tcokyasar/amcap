import math
from gurobipy import *
from datetime import datetime
import numpy as np
import sympy as sy
from prettytable import PrettyTable

def Cut_Generation(FA,FI,CF,CA,CI,CL,h,d,lmbda,mu,tauA,tauI,P_sub,P,I,S,
                   Sstar,B,M1,M2,M3,M4,totaltimelimit,seedno,desired_gap):
    ### BEGIN DEFINING SOLUTION PRINTER ###
    def solutionprinter():
        print('\x1b[1;23;42m' +'Number of AM machines at facilities:'+ '\x1b[0m')
        for s in range(1,S+1):
            for i in range(1,I+1):
                if x[s, i].x >=0.5:
                    print("Facility %s has %s AM machine(s)."%(i,int(round(s*x[s,i].x,1))))
        print("\n")
        print('\x1b[1;23;42m' +'The source for each product:'+ '\x1b[0m')
        for p in range(1,P+1):
            for i in range(1,I+1):
                if y[p,i].x >=0.5:
                    print("Product {} is sourced through facility {}.".format(p,i))
            if sum(y[p,i].x for i in range(1,I+1))<0.5:
                print("Product {} is sourced through the inventory option.".format(p))
        print('\n\x1b[1;23;43m' +"The LB objective value is $%s"% str(m.ObjBound) + '\x1b[0m\n')
        print('\n\x1b[1;23;44m' +'The UB objective value is $%s'%str(UB_calculator()[0]) +'\x1b[0m\n')
        print ('\n\x1b[1;23;45m' + 'The solution gap is currently %s'
               %(1-m.ObjBound/UB_calculator()[0]) + '\x1b[0m\n')
        return("")
    ### END DEFINING SOLUTION PRINTER ###
    
    ### BEGIN CREATING C_HAT CALCULATOR ###
    def UB_calculator():
        def Psicalc(rhoval, sval):
            return(((sval*rhoval)**sval)/((2*math.factorial(sval)
                    *sval**2*rhoval*(1-rhoval)**2
                    *(sum((sval*rhoval)**n/math.factorial(n) 
                    for n in range(0, sval))
                    +(sval*rhoval)**sval/
                    (math.factorial(sval)*(1-rhoval))))))
        rho_hat = {i:(sum(lmbda[p]/(mu[p]*sum(s*x[s,i].x 
                    for s in range(S+1)))*y[p,i].x 
                    for p in range(1,P+1)) if sum(s*x[s,i].x 
                    for s in range(S+1))>0 else 0) for i in range(1,I+1)}
        zeta_hat = {i:sum(lmbda[p]/(mu[p]**2)*y[p,i].x 
                    for p in range(1,P+1)) for i in range(1,I+1)}
        Wq_hat = {}
        for i in range(1,I+1):
            if rho_hat[i]>0 and int(round(sum(s*x[s,i].x for s in range(S+1)),0))>0:
                Wq_hat[i] = (zeta_hat[i]*Psicalc(rho_hat[i], 
                            int(round(sum(s*x[s,i].x for s in range(S+1)),0))))
            else:
                Wq_hat[i] = 0
        FA_hat = sum(sum(lmbda[p]*(CA[p]+CL[p,i]+d[p]*(tauA[p,i] 
                    + 1/mu[p] + Wq_hat[i]))*y[p,i].x
                    for p in range(1,P+1)) for i in range(1,I+1))
        FI_hat = sum(FI[p]*(1-sum(y[p,i].x for i in range(1,I+1))) 
                    for p in range (1,P+1))
        C_hat = sum((sum(s*x[s,i].x for s in range(S+1)))*CF[i] 
                    for i in range(1,I+1)) + FA_hat + FI_hat
        return(C_hat,Wq_hat)
    ### END CREATING C_HAT CALCULATOR ###

    ### BEGIN CALCULATING SOLUTION METRICS ###
    def solution_metrics_calculator(starttime):
        Total_machines_used = sum(s*x[s,i].x for s in range(S+1) for i in range(1,I+1))
        Num_assigned_to_A = int(sum(y[p,i].x for p in range(1,P+1) for i in range(1,I+1)))
        Num_assigned_to_I = int(P - Num_assigned_to_A)
        rho_list = [rho[i].x for i in range(1,I+1) if rho[i].x>0]
        Wq_list = [Wq[i].x*365*24 for i in range(1,I+1) if Wq[i].x>0 and rho[i].x>0]
        for i in range(1,I+1):
            print('Wq[%s]%s'%(i,Wq[i]))
        Avg_rho_i = (round(np.mean(rho_list),2) if len(rho_list)>0 else 0)
        Avg_Wq_i = (round(np.mean(Wq_list),2) if len(Wq_list)>0 else 0)
        Percent_of_units_by_AM = (sum(lmbda[p]*y[p,i].x 
              for p in range(1,P+1) for i in range(1,I+1))*100/sum(lmbda.values()))
        C = m.ObjBound
        C_hat,Wq_hat = UB_calculator()
        Avg_Wq = np.mean([i*365*24 for i in Wq_hat.values() if i>0])
        global solution_gap; solution_gap = 1-(C/C_hat)
        solution_time = (datetime.now()-starttime).total_seconds()
        return(Total_machines_used, Num_assigned_to_A, Num_assigned_to_I,
               Avg_rho_i, Avg_Wq, C, C_hat, Percent_of_units_by_AM,
               solution_gap, solution_time)
    ### END CALCULATING SOLUTION METRICS ###

    ### BEGIN PRINTING SOLUTION METRICS ###
    def table_printer(starttime):
        (Total_machines_used, Num_assigned_to_A, Num_assigned_to_I,
            Avg_rho_i, Avg_Wq_i, C, 
         C_hat, Percent_of_units_by_AM,
         solution_gap, solution_time) = solution_metrics_calculator(starttime)
        table = PrettyTable(['seed', '|P|', '|I|', '# machines',
                             'A', 'I', '%of units by AM', 'Avg. ρ', 'Avg. Wq (hr)', 
                             'C', 'C_hat', 'Gap', 'Time'])
        table.add_row([seedno, P, I, int(round(Total_machines_used,1)), Num_assigned_to_A,
            Num_assigned_to_I, round(Percent_of_units_by_AM,2), Avg_rho_i, 
            Avg_Wq_i, '{:,.1f}'.format(round(C,2)), 
            '{:,.1f}'.format(round(C_hat,2)), round(solution_gap,6), round(solution_time,2)])
        print(table); print('\n\n')
        return()
    ### END PRINTING SOLUTION METRICS ###

    ### BEGIN DEFINING (a,b) CALCULATOR THROUGH DERIVATIVE ###
    def Psi(k,rhoval):
        rhosy = sy.symbols('rhosy', real=True)
        nsy = sy.symbols('nsy', integer= True)
        Psi_rho_k = (((k*rhosy)**k)  / 
                    (2*math.factorial(k)*k**2*rhosy*(1-rhosy)**2
                    *(sum((k*rhosy)**n/math.factorial(n) for n in range(0, k))
                    +(k*rhosy)**k/(math.factorial(k)*(1-rhosy)))))
        d_Psi_rho_k = sy.diff(Psi_rho_k, rhosy).doit()
        b = float(d_Psi_rho_k.subs(rhosy, rhoval))
        a = float(Psi_rho_k.subs(rhosy, rhoval) 
                  - d_Psi_rho_k.subs(rhosy, rhoval)*rhoval)
        return([a,b])
    ### END DEFINING (a,b) CALCULATOR THROUGH DERIVATIVE ###

    ### BEGIN DEFINING CUT CONSTRAINT GENERATOR ###
    def cut_cons_gen():
        cut_dict = {i:Psi(int(round(sum(s*x[s,i].x for s in range(S+1)),1)),rho[i].x) 
                 + [int(round(sum(s*x[s,i].x for s in range(S+1)),1))] 
                 for i in range(1,I+1) if (x[0,i].x <= 0.9 and 
                 int(round(sum(s*x[s,i].x for s in range(S+1)),1))*rho[i].x>0)}
        for j in cut_dict.keys():
            for i in range(1,I+1):
                m.addConstr(Wq[i] >= cut_dict[j][0]
                        *quicksum(lmbda[p]/mu[p]**2*y[p,i] for p in range(1,P+1))
                        +cut_dict[j][1]*quicksum(lmbda[p]
                        *lmbda[p_prime]/(mu[p]**2*mu[p_prime]
                        *cut_dict[j][2])*z[p,p_prime,i] for p in range(1,P+1)
                        for p_prime in range(1,P+1)) - M4*(1-x[cut_dict[j][2],i]),
                        name = "cut_cons{}".format(j,i))
                print("I have just added a new cut " +
                      "based on a+b(rho)=%s+%s(rho) to location %s."%(cut_dict[j][0],
                        cut_dict[j][1],i))
        print("\nAt this iteration, we introduced %s cuts in total.\n"%(I*len(cut_dict.keys())))
        return(print("Now beginning to solve the new program with added cuts!\n"))
    ### END DEFINING CUT CONSTRAINT GENERATOR ###

    ### BEGIN CREATING THE MODEL ###
    m = Model("Multi-facility-Low-Volume")
    starttime = datetime.now()
    ### END CREATING THE MODEL ###

    ### BEGIN INTRODUCING VARIABLES ###
    x, y, z, rho, zeta, alpha, phi, Wq = {}, {}, {}, {}, {}, {}, {}, {}
    for p in range(1,P+1):
        for i in range(1,I+1):
            y[p,i] = m.addVar(vtype=GRB.BINARY,
                                name = "y%s" % str([p,i]))
            alpha[p,i] = m.addVar(vtype=GRB.CONTINUOUS,
                                name = "alpha%s" % str([p,i]))
            for p_prime in range(1,P+1):
                z[p,p_prime,i] = m.addVar(vtype=GRB.BINARY,
                                name = "z%s" % str([p,p_prime,i]))

    for i in range(1,I+1):
        Wq[i] = m.addVar(vtype=GRB.CONTINUOUS, name = "Wq%s" % str([i]))
        rho[i] = m.addVar(vtype=GRB.CONTINUOUS, ub = 0.99, name = "rho%s" % str([i]))
        zeta[i] = m.addVar(vtype=GRB.CONTINUOUS, name = "zeta%s" % str([i]))
        for s in range(S+1):
            x[s,i] = m.addVar(vtype=GRB.BINARY, name = "x%s" % str([s,i])) 
            phi[s,i] = m.addVar(vtype=GRB.CONTINUOUS, name = "phi%s" % str([s,i]))
    ### END INTRODUCING VARIABLES ###

    ### BEGIN CREATING THE OBJECTIVE FUNCTION ###
    m.setObjective(quicksum(CF[i]*quicksum(s*x[s,i] 
        for s in range(S+1)) for i in range(1,I+1)) # Fixed AM costs
        + quicksum(FA[p,i]*y[p,i]+alpha[p,i] for p in range(1,P+1)
        for i in range(1,I+1)) # Variable AM costs
        + quicksum(FI[p]*(1-quicksum(y[p,i] for i in range(1,I+1)))
        for p in range (1,P+1)) # Inventory option cost
        , GRB.MINIMIZE)
    ### END CREATING THE OBJECTIVE FUNCTION ###

    ### BEGIN ADDING CONSTRAINTS ###
    ### BEGIN SCENARIO 1 CONS ###
    for p in range(1,P+1):
        m.addConstr(quicksum(y[p,i] for i in range(1,I+1)) <= 1,
                    name = "p_is_sourced_from_up_to_one_loc{}".format(p))
    for i in range(1,I+1):
        m.addConstr(quicksum(x[s,i] for s in range(S+1)) == 1,
                    name="should_have_s_printers{}".format(i))
    ### END SCENARIO 1 CONS ###
    
    ### BEGIN SCENARIO 2 CONS ###
#     for p in range(1,P+1):
#         m.addConstr(quicksum(y[p,i] for i in range(1,I+1)) <= 1,
#                     name = "p_is_sourced_from_up_to_one_loc{}".format(p))
#     m.addConstr(quicksum(x[s,i] for s in range(S+1) for i in range(1,I+1)) <= 1,
#                     name="only_one_active_location")
    ### END SCENARIO 2 CONS ###
    
    ### BEGIN SCENARIO 3 CONS ###
#     I_p = {p:i for p in range(1,P+1) for i in range(1,I+1) if p in P_sub[i]}
#     for p in range(1,P+1):
#         m.addConstr(quicksum(y[p,i] for i in range(1,I+1) if i != I_p[p]) == 0)
#     for i in range(1,I+1):
#         m.addConstr(x[0,i] == 0)
#     for i in range(1,I+1):
#         m.addConstr(quicksum(x[s,i] for s in range(S+1)) == 1,
#                     name="should_have_s_printers{}".format(i))
    ### END SCENARIO 3 CONS ###
    
    ### BEGIN SCENARIO 4 CONS ###
#     for p in range(1,P+1):
#         m.addConstr(quicksum(y[p,i] for i in range(1,I+1)) == 1,
#                     name = "p_is_sourced_from_up_to_one_loc{}".format(p))
#     m.addConstr(quicksum(x[s,i] for s in range(S+1) for i in range(1,I+1)) <= 1,
#                     name="only_one_active_location")
    ### END SCENARIO 4 CONS ###
    
    ### BEGIN SCENARIO 5 CONS ###
#     for p in range(1,P+1):
#         m.addConstr(quicksum(y[p,i] for i in range(1,I+1)) <= 1,
#                     name = "p_is_sourced_from_up_to_one_loc{}".format(p))
#     for i in range(1,I+1):
#         m.addConstr(x[0,i] == 0)
#     I_p = {p:i for p in range(1,P+1) for i in range(1,I+1) if p in P_sub[i]}
#     for p in range(1,P+1):
#         m.addConstr(y[p,I_p[p]] == 1)
#     for i in range(1,I+1):
#         m.addConstr(quicksum(x[s,i] for s in range(S+1)) == 1,
#                     name="should_have_s_printers{}".format(i))
    ### END SCENARIO 5 CONS ###
    
    ## BEGIN SCENARIO 7 CONS ###
#     for p in range(1,P+1):
#         m.addConstr(quicksum(y[p,i] for i in range(1,I+1)) == 1,
#                     name = "p_is_sourced_from_up_to_one_loc{}".format(p))
#     for i in range(1,I+1):
#         m.addConstr(quicksum(x[s,i] for s in range(S+1)) == 1,
#                     name="should_have_s_printers{}".format(i))
    ## END SCENARIO 7 CONS ###

    for p in range(1,P+1):
        for i in range(1,I+1):
            m.addConstr(alpha[p,i] >= lmbda[p]*d[p]*Wq[i] - M1*(1-y[p,i]),
                    name = "alpha{}".format(p,i))
    for i in range(1,I+1):
        m.addConstr(zeta[i] == quicksum(lmbda[p]/(mu[p]**2)*y[p,i]
                    for p in range(1,P+1)), name="zeta_calc{}".format(i))
        m.addConstr(quicksum(s*phi[s,i] for s in range(S+1)) 
                    == quicksum(lmbda[p]*y[p,i]/mu[p] 
                    for p in range(1,P+1)), name = "phi_calc{}".format(i))
        for s in range(S+1):
            m.addConstr(rho[i]-M2*(1-x[s,i]) <= phi[s,i],
                        name="phi_si_1{}".format(s,i))
            m.addConstr(phi[s,i] <= M2*x[s,i],
                        name="phi_si_2{}".format(s,i))
            m.addConstr(phi[s,i]<=rho[i], name="phi_si_3{}".format(s,i))
    for p in range(1,P+1):
        for p_prime in range(1,P+1):
            for i in range(1,I+1):
                m.addConstr(z[p,p_prime,i] >= y[p,i] + y[p_prime,i] 
                        - int(1), name = "z_cons{}".format([p,p_prime,i]))
                m.addConstr(z[p,p_prime,i] <= y[p,i], 
                        name = "z_cons2{}".format([p,p_prime,i]))
                m.addConstr(z[p,p_prime,i] <= y[p_prime,i], 
                        name = "z_cons3{}".format([p,p_prime,i]))
    ### END ADDING CONSTRAINTS ###
    
    ### BEGIN SOLVING ###
    print("Beginning to solve the problem!\n")
    m.setParam('TimeLimit', totaltimelimit); m.setParam('MIPgap', 0.000001)
    m.update(); m.optimize(); print("\n\n")
    table_printer(starttime); print (solutionprinter())
    Remainingtime = totaltimelimit-(datetime.now()-starttime).total_seconds()
    ### END SOLVING ###
    
    ### BEGIN ADDING CUTS
    cut_iter_gap =[solution_gap]
    while solution_gap > desired_gap and Remainingtime > 0:
        starttimer = datetime.now()
        cut_cons_gen(); m.setParam('TimeLimit', Remainingtime)
        m.update; m.optimize(); print("\n\n")
        table_printer(starttimer); print(solutionprinter())
        cut_iter_gap.append(solution_gap)
        Remainingtime -= (datetime.now()-starttimer).total_seconds()
        if cut_iter_gap[-1] == cut_iter_gap[-2]:
            break
        
    x_sols = {(s,i):x[s,i].x for s in range(S+1) for i in range(1,I+1)}
    y_sols = {(p,i):y[p,i].x for p in range(1,P+1) for i in range(1,I+1)}
    rho_sols = {i:rho[i].x for i in range(1,I+1)}
    Wq_sols = {i:Wq[i].x for i in range(1,I+1)}
    return(x_sols,y_sols,rho_sols,Wq_sols,solution_metrics_calculator(starttime))