# Additive Manufacturing Capacity Allocation Problem over a Network
------------------

This project develops an optimization model to solve the
Additive Manufacturing Capacity Allocation Problem (AMCAP). We share the optimization
code and the data files
developed at the University of Tennessee.

This code and data files are supplementary materials for the manuscript 
"Additive Manufacturing Capacity Allocation Problem over a Network"
by Taner Cokyasar and Mingzhou Jin.

Authors and Contributors
--------------------------
Copyright (C) Taner Cokyasar - All Rights Reserved

Unauthorized copying of this file, via any medium is strictly prohibited

Proprietary and confidential

Written by Taner Cokyasar <tcokyasar@anl.gov>, July 2022

System Requirements
-------------------------
Python 3.8.8+

Gurobi 9.1.2+

Running
------------------------
To run, download the project and keep files in a single
same directory or change the directories as needed. You may run "main.py" to solve different
instances and access all the parameters and variables within this file. This file generates all required data using "data_generation_multi.py"
and "2014_us_cities.csv."
To solve different instances, you may change control parameters in main.py. This file
includes the Gurobi solver coding and the solution visualization code which generate maps and tables based 
on the solver output at each iteration. For smooth-visualization, we suggest using the Jupyter Notebook through Anaconda
interface. To do so, copy/paste "main.py" into a Jupyter Notebook file
with ".ipynb" and run the code through Jupyter.
